import axios from "axios";
import { gameDetailsURL, gameScreenshotsURL } from "../api";

export const loadGameDetails = (gameId) => async (dispatch) => {
  dispatch({
    type: "LOADING_DETAIL",
  });

  const gameDetails = await axios.get(gameDetailsURL(gameId));
  const gameScreenshots = await axios.get(gameScreenshotsURL(gameId));

  dispatch({
    type: "GET_DETAIL",
    payload: {
      game: gameDetails.data,
      screenshots: gameScreenshots.data,
    },
  });
};
