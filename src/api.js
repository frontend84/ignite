//Base URL
const base_url = "https://api.rawg.io/api/";

//Getting the date
const getCurrentMonth = () => {
  const month = new Date().getMonth() + 1;
  if (month < 10) {
    return `0${month}`;
  } else {
    return month;
  }
};

const getCurrentDay = () => {
  const day = new Date().getDate();
  if (day < 10) {
    return `0${day}`;
  } else {
    return day;
  }
};

//current day/month/year
const currentYear = new Date().getFullYear();
const currentMonth = getCurrentMonth();
const currentDay = getCurrentDay();
const currentDate = `${currentYear}-${currentMonth}-${currentDay}`;
const lastYear = `${currentYear - 1}-${currentMonth}-${currentDay}`;
const nextYear = `${currentYear + 1}-${currentMonth}-${currentDay}`;

//Popular, new and upcoming Games
const popular_games = `games?key=${process.env.REACT_APP_RAWG_API}&dates=${lastYear},${currentDate}&ordering=-rating&page_size=10`;
const upcoming_games = `games?key=${process.env.REACT_APP_RAWG_API}&dates=${currentDate},${nextYear}&ordering=-added&page_size=10`;
const newGames = `games?key=${process.env.REACT_APP_RAWG_API}&dates=${lastYear},${currentDate}&ordering=-released&page_size=10`;
export const popularGamesURL = () => `${base_url}${popular_games}`;
export const upcomingGamesURL = () => `${base_url}${upcoming_games}`;
export const newGamesURL = () => `${base_url}${newGames}`;

// get game details
const gameDetails = (gameId) => {
  return `games/${gameId}?key=${process.env.REACT_APP_RAWG_API}`;
};
export const gameDetailsURL = (gameId) => `${base_url}${gameDetails(gameId)}`;

//get screenshots
const gameScreenshots = (gameId) => {
  return `games/${gameId}/screenshots?key=${process.env.REACT_APP_RAWG_API}`;
};
export const gameScreenshotsURL = (gameId) =>
  `${base_url}${gameScreenshots(gameId)}`;

//Searched Game
const searchGame = (game_name) => {
  return `games?search=${game_name}&key=${process.env.REACT_APP_RAWG_API}`;
};
export const searchGameURL = (game_name) =>
  `${base_url}${searchGame(game_name)}&page_size=9`;
